<%--
  Created by IntelliJ IDEA.
  User: nail
  Date: 18.10.2019
  Time: 19:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="registration-page">
    <form class="registration-form" method="POST" action="registration">
        <h2>Регистрация</h2>
        <c:if test="${notif ne null}">
            <div class="notif">
                <span>${notif}</span>
            </div>
        </c:if>
        <div class="input-block">
            <label for="login">Логин</label>
            <input type="text" name="login" id="login"/>
        </div>
        <div class="input-block">
            <label for="email">E-Mail</label>
            <input type="email" name="email" id="email"/>
        </div>
        <div class="input-block">
            <label for="password">Пароль</label>
            <input type="password" name="password" id="password"/>
        </div>
        <div class="input-block">
            <label for="passwordRepeat">Повторите пароль</label>
            <input type="password" name="passwordRepeat" id="passwordRepeat"/>
        </div>
        <div class="submit-button-block">
            <button type="submit" class="submit-button">Зарегистрироваться</button>
        </div>
    </form>
</div>
