package ru.naiu.javalab.consts;

public class Views {

    //Base
    private static final String VIEWS_BASE = "/WEB-INF/views";

    //Common
    public static final String INDEX = "/WEB-INF/index.jsp";

    //User
    private static final String REGISTRATION_BASE = VIEWS_BASE + "/user";
    public static final String REGISTRATION = REGISTRATION_BASE + "/registration.jsp";

    //Articles
    private static final String ARTICLES_BASE = VIEWS_BASE + "/article";
    public static final String ARTICLES = ARTICLES_BASE + "/index.jsp";
    public static final String ALL_ARTICLES = ARTICLES_BASE + "/all-articles.jsp";
    public static final String PRIVATE_ARTICLES = "/WEB-INF/private/private-article.jsp";
}
