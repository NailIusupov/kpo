package ru.naiu.javalab.models.session;


import ru.naiu.javalab.models.Articles;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ArticlesFacade extends AbstractFacade<Articles> {
    @PersistenceContext(unitName = "myblogPU")
    private EntityManager em;
    private Articles articles;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ArticlesFacade() {
        super(Articles.class);
    }

    public List getFistArticles() {
        Query query = em.createQuery("SELECT articles from Articles articles");
        query.setMaxResults(3);
        return query.getResultList();
    }
    
}
