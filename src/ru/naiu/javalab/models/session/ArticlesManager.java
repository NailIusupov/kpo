package ru.naiu.javalab.models.session;

import ru.naiu.javalab.models.Articles;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ArticlesManager {

    @PersistenceContext(unitName = "myblogPU")
    private EntityManager entityManager;

    @Resource
    private SessionContext sessionContext;

    @TransactionAttribute
    public Integer addArticle(final String title, final String text) {
        try {
            newArticle(title, text);
            return 0;
        } catch (Exception e) {
            sessionContext.setRollbackOnly();
            e.printStackTrace();
            return 1;
        }
    }

    private void newArticle(String title, String text) {
        Articles article = new Articles();
        article.setTitle(title);
        article.setText(text);
        article.setDate(new Date());

        entityManager.persist(article);
    }
}
