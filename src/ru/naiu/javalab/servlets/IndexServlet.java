package ru.naiu.javalab.servlets;

import ru.naiu.javalab.consts.Views;
import ru.naiu.javalab.models.session.ArticlesFacade;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "index", urlPatterns = "/", loadOnStartup = 1)
public class IndexServlet extends HttpServlet {
    @EJB
    ArticlesFacade articlesFacade;

    @Override
    public void init() throws ServletException {
        getServletContext().setAttribute("articles", articlesFacade.getFistArticles());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher(Views.INDEX).forward(req, res);
    }
}
