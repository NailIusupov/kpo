package ru.naiu.javalab.servlets.articles;

import ru.naiu.javalab.consts.Views;
import ru.naiu.javalab.models.session.ArticlesFacade;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "allArticles", urlPatterns = "/all-articles")
public class AllArticles extends HttpServlet {

    @EJB
    ArticlesFacade articlesFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("articles", articlesFacade.findAll());
        request.getRequestDispatcher(Views.ALL_ARTICLES).forward(request, response);
    }
}
