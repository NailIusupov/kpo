package ru.naiu.javalab.servlets.articles;

import ru.naiu.javalab.consts.Views;
import ru.naiu.javalab.models.Articles;
import ru.naiu.javalab.models.session.ArticlesFacade;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "article", urlPatterns = "/article")
public class ArticleServlet extends HttpServlet {

    private static String ARTICLE_ID = "article_id";

    @EJB
    private ArticlesFacade articlesFacade;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        int articleId = Integer.parseInt(req.getParameter(ARTICLE_ID));

        try {
            Articles article = articlesFacade.find(articleId);
            req.setAttribute("article", article);
        } catch (Exception e) {
            e.printStackTrace();
        }

        req.getRequestDispatcher(Views.ARTICLES).forward(req, res);
    }
}
