package ru.naiu.javalab.servlets.articles;

import ru.naiu.javalab.consts.Views;
import ru.naiu.javalab.models.session.ArticlesManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(name = "PrivateArticlesServlet", urlPatterns = {"/private"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"private"}))
public class PrivateArticlesServlet extends HttpServlet {

    @EJB
    ArticlesManager articlesManager;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("name", request.getUserPrincipal().getName());
        request.getRequestDispatcher(Views.PRIVATE_ARTICLES).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = null, text = null;
        Enumeration<String> parameters = req.getParameterNames();

        while (parameters.hasMoreElements()) {
            String parameter = parameters.nextElement();

            switch (parameter) {
                case "title": {
                    title = req.getParameter(parameter);
                    break;
                }
                case "text": {
                    text = req.getParameter(parameter);
                    break;
                }
                default: break;
            }
        }

        Integer codeOperation = articlesManager.addArticle(title, text);

        if(codeOperation != 0) {
            req.setAttribute("notif", "Код завершения операции: " + codeOperation);
        } else {
            resp.sendRedirect(req.getContextPath() + "/all-articles");
        }
    }
}
