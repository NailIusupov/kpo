package ru.naiu.javalab.servlets.user;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "UserAuth", urlPatterns = {"/logout"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"private"}))
public class UserAuth extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session!= null){
            session.invalidate();
        }
        response.sendRedirect(request.getContextPath());
    }
}
