package ru.naiu.javalab.servlets.user;

import ru.naiu.javalab.consts.Views;
import ru.naiu.javalab.models.session.UsersManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;

@WebServlet(name = "userReg", urlPatterns = {"/registration"})
public class UserReg extends HttpServlet {

    @EJB
    UsersManager usersManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher(Views.REGISTRATION).forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = null, password = null, passwordRepeat = null;
        HashMap<String, String[]> contacts = new HashMap<>();
        Enumeration<String> parameters = req.getParameterNames();

        while (parameters.hasMoreElements()) {
            String parameter = parameters.nextElement();

            switch (parameter) {
                case "login": {
                    login = req.getParameter(parameter);
                    break;
                }
                case "password": {
                    password = req.getParameter(parameter);
                    break;
                }
                case "passwordRepeat": {
                    passwordRepeat = req.getParameter(parameter);
                    break;
                }
                default: contacts.put(parameter, req.getParameterValues(parameter));
            }
        }

        Integer codeOperation = usersManager.addUser(login, password, passwordRepeat, contacts);

        if(codeOperation != 0) {
            req.setAttribute("notif", "Код завершения операции: " + codeOperation);
        } else {
            req.setAttribute("notif", "Пользователь " + login + " успешно создан!");
        }

        req.getRequestDispatcher(Views.REGISTRATION).forward(req, resp);

    }
}
