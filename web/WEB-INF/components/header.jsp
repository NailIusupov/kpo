<%--
  Created by IntelliJ IDEA.
  User: nail
  Date: 18.10.2019
  Time: 18:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="resources/css/user.css">
    <link rel="stylesheet" type="text/css" href="resources/css/components.css">
    <link rel="stylesheet" type="text/css" href="resources/css/default.css">
    <link rel="stylesheet" type="text/css" href="resources/css/articles.css">
    <link rel="stylesheet" type="text/css" href="resources/css/full-article.css">
    <title>КПО</title>
</head>
<body>
<header class="main-header">
    <div class="main-header-content container">
        <div class="logo"><a href="${pageContext.request.contextPath}/">Главная</a></div>
        <nav class="main-header__auth-block">
            <a href="${pageContext.request.contextPath}/private" class="auth-link enter">Добавить статью</a>
            <a href="${pageContext.request.contextPath}/registration" class="auth-link registration">Регистрация</a>
            <a href="${pageContext.request.contextPath}/logout" class="auth-link exit">Выход</a>
        </nav>
    </div>
</header>
