<%--
  Created by IntelliJ IDEA.
  User: nail
  Date: 14.11.2019
  Time: 21:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="login-page">
    <form action="j_security_check" method="POST" class="login-form">
        <h2>Авторизация</h2>
        <p class="input-block"><strong>Ваш логин:</strong>
            <input placeholder="Введите логин" type="text" size="20" name="j_username"></p>
        <p class="input-block"><strong>Пароль:</strong>
            <input placeholder="Введите пароль" type="password" size="20" name="j_password"></p>
        <p><input type="submit" value="Авторизоваться" class="submit-button"></p>
    </form>
</section>
