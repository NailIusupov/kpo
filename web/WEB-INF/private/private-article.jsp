<%--
  Created by IntelliJ IDEA.
  User: nail
  Date: 14.11.2019
  Time: 21:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="add-article-page">
    <form class="add-article-form" method="post">
        <label>
            <input class="add-article-form__title-input" placeholder="Заголовок" name="title">
        </label>
        <label>
            <textarea placeholder="Текст" name="text"></textarea>
        </label>
        <input type="submit" class="add-article-form__submit-button" value="Опубликовать">
    </form>
</section>
