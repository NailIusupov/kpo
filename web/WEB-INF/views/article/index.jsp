<%--
  Created by IntelliJ IDEA.
  User: nail
  Date: 18.10.2019
  Time: 19:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<section class="full-article-page">
    <article class="full-article">
        <h1 class="full-article__header">${article.title}</h1>
        <p class="full-article__text">
            ${article.text}
        </p>
        <div class="full-article__footer">
            <span class="full-article__footer__date"><b>Дата статьи:</b> ${article.date}</span>
        </div>
    </article>
</section>