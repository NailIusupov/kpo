<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="article-page">
  <div class="article-page-content container">
    <aside class="extended-info">
      <h2>Темы статей</h2>
      <nav class="extended-info__themes">
        <c:forEach var="article" items="${articles}">
          <a href="${pageContext.request.contextPath}/article?article_id=${article.id}">${article.title}</a>
        </c:forEach>
      </nav>
    </aside>
    <main class="articles">
      <c:forEach var="article" items="${articles}">
        <article class="article-long">
          <header class="article-long__header">
            <a href="${pageContext.request.contextPath}/article?article_id=${article.id}"><h2>${article.title}</h2></a>
            <div class="article-long__extended-info">
              <h3>Дата: ${article.date}</h3>
            </div>
          </header>
          <article class="article-long__text">
            ${fn:substring(article.text,0,300)}...
          </article>
        </article>
      </c:forEach>
      <a href="${pageContext.request.contextPath}/all-articles">Больше статей</a>
    </main>
  </div>
</div>